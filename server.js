const express = require('express')
const app = express()
const port = process.env.PORT || 80

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/students', (req, res) => {
    res.json({
        students: {
            0913182: {name: 'Kamran Fekri'}
        }
    })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))